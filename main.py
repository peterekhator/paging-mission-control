from datetime import datetime
import json

data_file = 'data.txt'

with open(data_file) as f:
    data = []
    for row in f.read().split('\n'):
        if row != '':
            data.append(row)

alerts = []
for i in range(len(data)):
    time = data[i].split('|')[0]
    ref_datetime = datetime.strptime(time, '%Y%m%d %H:%M:%S.%f')
    sattelite_id = data[i].split('|')[1]

    five_minutes_interval_data = []
    for j in range(i, len(data)):
        j_time, j_sattelite_id, rhl, yhl, yll, rll, rv, component = data[j].split('|')
        j_datetime = datetime.strptime(j_time, '%Y%m%d %H:%M:%S.%f')

        interval = j_datetime - ref_datetime
        minutes = interval.seconds / 60
        if (j_sattelite_id == sattelite_id) and (minutes <= 5):
            five_minutes_interval_data.append({
                'red-low-limit': float(rll),
                'red-high-limit': float(rhl),
                'raw-value': float(rv),
                'component': component
            })

    batt_rll_counter, thermostat_rhl_counter = 0, 0
    for item in five_minutes_interval_data:
        rhl, rll, rv = item['red-high-limit'], item['red-low-limit'], item['raw-value']
        if (item['component']=='BATT') and (rv<rll):
            batt_rll_counter += 1
        elif (item['component']=='TSTAT') and (rv>rhl):
            thermostat_rhl_counter += 1

    if batt_rll_counter >=3:
        alerts.append({
            'satteliteId': sattelite_id,
            'severity': 'RED LOW',
            'component': 'BATT',
            'timestamp': ref_datetime.strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] + 'Z'
        })

    elif thermostat_rhl_counter >= 3:
        alerts.append({
            'satteliteId': sattelite_id,
            'severity': 'RED HIGH',
            'component': 'TSTAT',
            'timestamp': ref_datetime.strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] + 'Z'
        })

print(json.dumps(alerts, indent=4))
